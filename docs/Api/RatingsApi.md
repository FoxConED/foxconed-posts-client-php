# Ensi\PostsClient\RatingsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRating**](RatingsApi.md#createRating) | **POST** /posts/ratings | Создание объекта типа Rating
[**deleteRating**](RatingsApi.md#deleteRating) | **DELETE** /posts/ratings | Удаление объекта типа Rating
[**revertRatings**](RatingsApi.md#revertRatings) | **POST** /posts/ratings/{id}:revert | Сменить рейтинг для объектов типа Rating
[**searchRatings**](RatingsApi.md#searchRatings) | **POST** /posts/ratings:search | Поиск объектов типа Rating



## createRating

> \Ensi\PostsClient\Dto\RatingResponse createRating($rating_for_create)

Создание объекта типа Rating

Создание объекта типа Rating

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\RatingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$rating_for_create = new \Ensi\PostsClient\Dto\RatingForCreate(); // \Ensi\PostsClient\Dto\RatingForCreate | 

try {
    $result = $apiInstance->createRating($rating_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingsApi->createRating: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rating_for_create** | [**\Ensi\PostsClient\Dto\RatingForCreate**](../Model/RatingForCreate.md)|  |

### Return type

[**\Ensi\PostsClient\Dto\RatingResponse**](../Model/RatingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteRating

> \Ensi\PostsClient\Dto\EmptyDataResponse deleteRating()

Удаление объекта типа Rating

Удаление объекта типа Rating

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\RatingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->deleteRating();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingsApi->deleteRating: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\PostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## revertRatings

> \Ensi\PostsClient\Dto\RatingResponse revertRatings($id)

Сменить рейтинг для объектов типа Rating

Сменить рейтинг для объектов типа Rating

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\RatingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->revertRatings($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingsApi->revertRatings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PostsClient\Dto\RatingResponse**](../Model/RatingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchRatings

> \Ensi\PostsClient\Dto\SearchRatingsResponse searchRatings($search_ratings_request)

Поиск объектов типа Rating

Поиск объектов типа Rating

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\RatingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_ratings_request = new \Ensi\PostsClient\Dto\SearchRatingsRequest(); // \Ensi\PostsClient\Dto\SearchRatingsRequest | 

try {
    $result = $apiInstance->searchRatings($search_ratings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingsApi->searchRatings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_ratings_request** | [**\Ensi\PostsClient\Dto\SearchRatingsRequest**](../Model/SearchRatingsRequest.md)|  |

### Return type

[**\Ensi\PostsClient\Dto\SearchRatingsResponse**](../Model/SearchRatingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

