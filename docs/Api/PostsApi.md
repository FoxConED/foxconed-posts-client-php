# Ensi\PostsClient\PostsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPost**](PostsApi.md#createPost) | **POST** /posts/posts | Создание объекта типа Post
[**deletePost**](PostsApi.md#deletePost) | **DELETE** /posts/posts/{id} | Удаление объекта типа Post
[**getPost**](PostsApi.md#getPost) | **GET** /posts/posts/{id} | Получение объекта типа Post
[**patchPost**](PostsApi.md#patchPost) | **PATCH** /posts/posts/{id} | Обновления части полей объекта типа Post
[**replacePost**](PostsApi.md#replacePost) | **PUT** /posts/posts/{id} | Замена объекта типа Post
[**searchPosts**](PostsApi.md#searchPosts) | **POST** /posts/posts:search | Поиск объектов типа Post



## createPost

> \Ensi\PostsClient\Dto\PostResponse createPost($post_for_create)

Создание объекта типа Post

Создание объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$post_for_create = new \Ensi\PostsClient\Dto\PostForCreate(); // \Ensi\PostsClient\Dto\PostForCreate | 

try {
    $result = $apiInstance->createPost($post_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->createPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **post_for_create** | [**\Ensi\PostsClient\Dto\PostForCreate**](../Model/PostForCreate.md)|  |

### Return type

[**\Ensi\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePost

> \Ensi\PostsClient\Dto\EmptyDataResponse deletePost($id)

Удаление объекта типа Post

Удаление объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePost($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->deletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPost

> \Ensi\PostsClient\Dto\PostResponse getPost($id, $include)

Получение объекта типа Post

Получение объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getPost($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->getPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPost

> \Ensi\PostsClient\Dto\PostResponse patchPost($id, $post_for_patch)

Обновления части полей объекта типа Post

Обновления части полей объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$post_for_patch = new \Ensi\PostsClient\Dto\PostForPatch(); // \Ensi\PostsClient\Dto\PostForPatch | 

try {
    $result = $apiInstance->patchPost($id, $post_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->patchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **post_for_patch** | [**\Ensi\PostsClient\Dto\PostForPatch**](../Model/PostForPatch.md)|  |

### Return type

[**\Ensi\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replacePost

> \Ensi\PostsClient\Dto\PostResponse replacePost($id, $post_for_replace)

Замена объекта типа Post

Замена объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$post_for_replace = new \Ensi\PostsClient\Dto\PostForReplace(); // \Ensi\PostsClient\Dto\PostForReplace | 

try {
    $result = $apiInstance->replacePost($id, $post_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->replacePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **post_for_replace** | [**\Ensi\PostsClient\Dto\PostForReplace**](../Model/PostForReplace.md)|  |

### Return type

[**\Ensi\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPosts

> \Ensi\PostsClient\Dto\SearchPostsResponse searchPosts($search_posts_request)

Поиск объектов типа Post

Поиск объектов типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_posts_request = new \Ensi\PostsClient\Dto\SearchPostsRequest(); // \Ensi\PostsClient\Dto\SearchPostsRequest | 

try {
    $result = $apiInstance->searchPosts($search_posts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->searchPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_posts_request** | [**\Ensi\PostsClient\Dto\SearchPostsRequest**](../Model/SearchPostsRequest.md)|  |

### Return type

[**\Ensi\PostsClient\Dto\SearchPostsResponse**](../Model/SearchPostsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

