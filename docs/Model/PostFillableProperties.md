# # PostFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Идентификатор автора | [optional] 
**title** | **string** | Заголовок | [optional] 
**preview** | **string** | Превью поста | [optional] 
**content** | **string** | Контент поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


