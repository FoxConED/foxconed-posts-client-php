# # PostReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | [optional] 
**user_id** | **int** | Идентификатор автора поста | [optional] 
**title** | **string** | Заголовок поста | [optional] 
**preview** | **string** | Превью поста | [optional] 
**content** | **string** | Контент поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


