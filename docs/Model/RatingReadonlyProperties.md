# # RatingReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор рейтинга | [optional] 
**user_id** | **int** | Идентификатор автора рейтинга | [optional] 
**post_id** | **int** | Идентификатор поста | [optional] 
**like** | **bool** | Оценка поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


