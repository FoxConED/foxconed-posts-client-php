<?php
/**
 * RatingFillablePropertiesTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PostsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Laravel-Swagger REST-API
 *
 * Просто API
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: example@mail.com
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\PostsClient;

use PHPUnit\Framework\TestCase;

/**
 * RatingFillablePropertiesTest Class Doc Comment
 *
 * @category    Class
 * @description RatingFillableProperties
 * @package     Ensi\PostsClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class RatingFillablePropertiesTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "RatingFillableProperties"
     */
    public function testRatingFillableProperties()
    {
    }

    /**
     * Test attribute "user_id"
     */
    public function testPropertyUserId()
    {
    }

    /**
     * Test attribute "post_id"
     */
    public function testPropertyPostId()
    {
    }

    /**
     * Test attribute "like"
     */
    public function testPropertyLike()
    {
    }
}
