<?php

namespace Ensi\PostsClient;

class PostsClientProvider
{
    /** @var string[] */
    public static $apis = ['\Ensi\PostsClient\Api\PostsApi', '\Ensi\PostsClient\Api\RatingsApi'];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\PostsClient\Dto\EmptyDataResponse',
        '\Ensi\PostsClient\Dto\Error',
        '\Ensi\PostsClient\Dto\ErrorResponse',
        '\Ensi\PostsClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\PostsClient\Dto\PaginationTypeEnum',
        '\Ensi\PostsClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\PostsClient\Dto\Post',
        '\Ensi\PostsClient\Dto\PostFillableProperties',
        '\Ensi\PostsClient\Dto\PostForCreate',
        '\Ensi\PostsClient\Dto\PostForPatch',
        '\Ensi\PostsClient\Dto\PostForReplace',
        '\Ensi\PostsClient\Dto\PostReadonlyProperties',
        '\Ensi\PostsClient\Dto\PostResponse',
        '\Ensi\PostsClient\Dto\Rating',
        '\Ensi\PostsClient\Dto\RatingFillableProperties',
        '\Ensi\PostsClient\Dto\RatingForCreate',
        '\Ensi\PostsClient\Dto\RatingReadonlyProperties',
        '\Ensi\PostsClient\Dto\RatingResponse',
        '\Ensi\PostsClient\Dto\RequestBodyCursorPagination',
        '\Ensi\PostsClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\PostsClient\Dto\RequestBodyPagination',
        '\Ensi\PostsClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\PostsClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\PostsClient\Dto\ResponseBodyPagination',
        '\Ensi\PostsClient\Dto\SearchPostsRequest',
        '\Ensi\PostsClient\Dto\SearchPostsResponse',
        '\Ensi\PostsClient\Dto\SearchPostsResponseMeta',
        '\Ensi\PostsClient\Dto\SearchRatingsRequest',
        '\Ensi\PostsClient\Dto\SearchRatingsResponse',
        '\Ensi\PostsClient\Dto\ModelInterface',
    ];

    /** @var string */
    public static $configuration = '\Ensi\PostsClient\Configuration';
}
